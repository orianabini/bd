<!--Encabezado-->
<!DOCTYPE html> 
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <!--BOOTSTRAP-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--FONT AWESOME-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/stiles.css">
    </head>
    <!--BOOTSTRAP-->
    <nav class = "navbar navbar-dark bg-dark"> <!--clase de bootstrap-->
    <header class="p-1 bg-black text-white container">
        <div class = "container"> <!-- Agregamos esta clase para el contenido de la navegacion este centrado-->
          <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
              <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
            </a>
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="index.php" class="nav-link px-1 text-white">Inicio</a></li>
                <li><a href="php/listadoVentas.php" class="nav-link px-1 text-white">Lista ventas</a></li>
        
                <li><a href="php/EmpleadosAlta.php" class="nav-link px-1 text-white">Alta empleado</a></li>
                <li><a href="php/EmpleadosModificar.php" class="nav-link px-1 text-white">Modificar empleado</a></li>
                <li><a href="php/EmpleadosBorrar.php" class="nav-link px-1 text-white">Borrar empleado</a></li>
        
                <li><a href="php/ProductosAlta.php" class="nav-link px-1 text-white">Alta producto</a></li>
                <li><a href="php/ProductosModificar.php" class="nav-link px-1 text-white">Modificar producto</a></li>
                <li><a href="php/ProductosBorrar.php" class="nav-link px-1 text-white">Borrar producto</a></li>
              
                <li><a href="php/VentasAlta.php" class="nav-link px-1 text-white">Alta venta</a></li>
                <li><a href="php/VentasModificar.php" class="nav-link px-1 text-white">Modificar venta</a></li>
                <li><a href="php/VentasBorrar.php" class="nav-link px-1 text-white">Borrar venta</a></li>
            </ul>
        </div>
      </header>
      </div>
    </nav>
<?php
    if(isset ($_POST['id_empleado'])) {
        include ("php/conexion.php");
    $id_empleado = $_POST['id_empleado'];
    $nombre = $_POST ['nombre'];
    $contrasena = $_POST ['contrasena'];

    $query = "INSERT INTO usuarios(id_empleado, nombre, contrasena) 
    VALUES ('$id_empleado', '$nombre', '$contrasena')";

    $ejecutar = mysqli_query($conexion, $query);
    }
?>
<!--
<!DOCTYPE html>
<header>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../CRE/assets/css/estilos.css">
    <title> Inicio </title>
       
        <ul id="menu">
                <li> <a href="index.php"> Inicio</a></li>
                <li> <a> Empleados</a>
                    <ul>
                        <li> <a href="php/EmpleadosAlta.php"> Alta</a></li>
                        <li> <a href="php/EmpleadosBorrar.php"> Borrar</a></li>
                        <li> <a href="php/EmpleadosModificar.php"> Modificar</a></li>
                    </ul>
                    </li>        
        </ul> 
</header>
-->
<!--
<body>
        <form method="POST" id ="formulario">

        <h3> Ingrese Usuario:</h3>
            ID DNI: <input type="text" placeholder="Ingrese DNI" name="id_empleado" required><br><br>
            Nombre: <input type="text" placeholder="Ingrese Nombre" name="nombre" required ><br><br>
            Contraseña: <input type="password" placeholder="Ingrese Contraseña" name="contrasena" required><br><br>
            <button>Entrar</button>
</body>
-->
<body class="text-center">
<main class="form-signin">
<div class="card card-body">
    <form method="POST" id ="formulario">   
        <h1 class="h3 mb-3 fw-normal">Ingrese empleado</h1>
        <div class = "contenedor_todo">
            <div class="form-floating">
                <input type="text" class="form-control" id="id_empleado" name="id_empleado" placeholder="ID" required>
                <label for="id_empleado">ID</label>
            </div>
            <div class="form-floating">
                <input type="text" class="form-control" id="nombre" name="nombre"placeholder="Nombre" required>
                <label for="nombre">Nombre</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder="Contraseña" required>
                <label for="contrasena">Contraseña</label>
            </div>
            <div class="checkbox mb-3">
                <label>
                <input type="checkbox" value="remember-me"> Recordarme
                </label>
            </div>
        </div>
        <button class="w-10 btn btn-lg btn-secondary" type="submit">Entrar</button>
    </form>
</div>
</main>
</body>