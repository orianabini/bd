<?php
    include("header.php");
    if (isset ($_POST ['id_empleado'])) {
        include "conexion.php";
    $id_empleado = $_POST ['id_empleado'];

    $query = "DELETE FROM empleados WHERE id_empleado = '$id_empleado'";

    $ejecutar = mysqli_query ($conexion, $query);
    } 
?>

 <body class="text-center">
<main class="form-signin">
<div class="card card-body">
    <form method="POST" id ="formulario">
        <h1 class="h3 mb-3 fw-normal">Ingrese empleado a borrar</h1>
        <div class = "contenedor_todo">
            <div class="form-floating">
                <input type="number" class="form-control" id="id_empleado" name="id_empleado" placeholder="ID" required>
                <label for="id_empleado">ID</label>
            </div>
        </div>
        <button class="w-10 btn btn-lg btn-secondary" type="submit">Borrar empleado</button>
    </form>
</div>
</main>
</body>