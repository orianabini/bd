<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!--FONT AWESOME-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="../assets/css/stiles.css">
    <body>

<nav class = "navbar navbar-dark bg-dark"> <!--clase de bootstrap-->
<header class="p-1 bg-black text-white container">
    <div class = "container"> <!-- Agregamos esta clase para el contenido de la navegacion este centrado-->
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
          <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
        </a>
        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <li><a href="../index.php" class="nav-link px-1 text-white">Inicio</a></li>
          <li><a href="listadoVentas.php" class="nav-link px-1 text-white">Lista ventas</a></li>

          <li><a href="EmpleadosAlta.php" class="nav-link px-1 text-white">Alta empleado</a></li>
          <li><a href="EmpleadosModificar.php" class="nav-link px-1 text-white">Modificar empleado</a></li>
          <li><a href="EmpleadosBorrar.php" class="nav-link px-1 text-white">Borrar empleado</a></li>

          <li><a href="ProductosAlta.php" class="nav-link px-1 text-white">Alta producto</a></li>
          <li><a href="ProductosModificar.php" class="nav-link px-1 text-white">Modificar producto</a></li>
          <li><a href="ProductosBorrar.php" class="nav-link px-1 text-white">Borrar producto</a></li>

          <li><a href="VentasAlta.php" class="nav-link px-1 text-white">Alta venta</a></li>
          <li><a href="VentasModificar.php" class="nav-link px-1 text-white">Modificar venta</a></li>
          <li><a href="VentasBorrar.php" class="nav-link px-1 text-white">Borrar venta</a></li>
        </ul>
    </div>
  </header>
  </div>
</nav>